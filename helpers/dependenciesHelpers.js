/**
 * This method looks for all the dependencies recursively
 * If it detects that depencies are in a loop, throws an exception
 * 
 * @param {String} module  The module going to be installed
 * @param {Function} fn Required function to get all the depencies for a given module
 * @return {Array} Contains all the dependencies that needs to be installed, order 
 */

const buildModuleDependencies = (module, fn) => {
    const getDependencies = (name) => fn()(name).requires.sort();
    let dependenciesList = [];

    try {
        dependenciesList  = [getDependencies(module)];
    } catch (message) {
        throw Error('ERROR_MODULE_UNKNOWN');
    }

    var lookForDependencies = (list) => {
        list.forEach((mod) => {
            let dependencies = null;
            dependencies = getDependencies(mod);
        
            if(dependenciesList.includes(dependencies)) throw Error('ERROR_MODULE_DEPENDENCIES');
        
            if ( dependencies.length) {
                dependenciesList.unshift(dependencies);
                lookForDependencies(dependencies);
            }
        });
    };

    lookForDependencies(dependenciesList[0]);
    return [].concat.apply([], dependenciesList);
};

// Install all the  dependencies and wait for them to finish
async function installAllDependencies(module, installer, modulesFactory) {
    const tasks = buildModuleDependencies(module, modulesFactory);
    if(!tasks.length) return;

    for (const task of tasks) {
        await installer(task);
    }
}

module.exports = {
    buildModuleDependencies,
    installAllDependencies
};